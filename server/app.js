/**
 * Server端入口模块
 */
let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let logger = require('morgan');
// 跨域请求模块
let cors = require('cors');
// 加载数据库对象
let db = require('./src/database');

let errorHandler = require('./error.handler');

let uploadRouter = require('./routes/upload');

let app = express();

app.use(logger('dev'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/upload', uploadRouter);

errorHandler(app)

module.exports = app;
