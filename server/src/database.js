/**
 * Mongodb数据库连接类
 * 
 * @date 2018-08-02
 */
let mongoose = require('mongoose')
let chalk = require('chalk')

// @todo
// Must replace when your deploy
const SERVER = '127.0.0.1:27017'
const DATABASE = 'cargo'
// const COLLECTION = 'records'

class DataBase {
  constructor () {
    this._connect()
  }

  _connect () {
    mongoose.connect(`mongodb://${SERVER}/${DATABASE}`, {
      useNewUrlParser: true
    })
    .then(() => {
      console.log(chalk.white.bgGreen.bold('DATABASE CONNECTION SUCCESSFUL'))
    })
    .catch(err => {
      console.error(chalk.white.bgRed.bold('DATABASE CONNECTION ERROR'))
    })
  }
}

module.exports = new DataBase()