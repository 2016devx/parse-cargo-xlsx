/**
 * 运单记录类
 */
let mongoose = require('mongoose')
let validator = require('validator')

let itemSchema = new mongoose.Schema({
  // 起运日期
  start_run_date: {
    type: String,
    required: true
  },
  // 被保险人
  insured_name: {
    type: String,
    required: true
  },
  // 保险标的/货物
  insure_product: {
    type: String,
    required: true
  },
  // 数量
  count: {
    type: String,
    required: true
  },
  // 船名/航次
  boat_flight_no: {
    type: String,
    required: true
  },
  // 起运地
  start_addr: {
    type: String,
    required: true
  },
  // 目的地
  end_addr: {
    type: String,
    required: true
  },
  // 运单号 
  transport_no: {
    type: String,
    required: true
  },
  // 柜台/封号
  box_no: {
    type: String,
    required: true
  },
  // 保险金额
  insure_amount: {
    type: String,
    required: true
  }, 
  // 保费
  premium: {
    type: String,
    required: true
  },
  // 费率
  fee_rate: {
    type: String,
    default: '0.00%'
  }
})

module.exports = mongoose.model('Records', itemSchema)