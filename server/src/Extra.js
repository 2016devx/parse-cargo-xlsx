/**
 * Extra类
 * 
 * 策略模式中的Context类
 */
class Extra {
    constructor () {
        this.records = null
        // 委托对象
        this.strategy = null
    }

    setRecords (records) {
        this.records = records
    }

    setStrategy (strategy) {
        this.strategy = strategy
    }

    getResult () {
        // 委托策略对象对records进行解析
        return this.strategy.parse(this.records)
    }
}

module.exports = Extra