/**
 * 洋浦中惠策略类
 * 
 * - 负责解析Excel具体的内容
 */
let parseDate = require('../util').parseDate

class YangPuParser 
{
    /**
     * Class Constructor
     * @param {Object} records Excel解析出来的records对象
     * 
     * @return this
     */
    constructor () {
        this.records = null
    }

    /**
     * 将records中的数组item转换为对象
     */
    toArray () {
        let arr = this.records.map((r, index) => {
            return {
                insured_name: r[1],
                start_run_date: parseDate(r[2]),
                insure_product: r[3],
                count: r[4],
                boat_flight_no: r[5],
                start_addr: r[6],
                end_addr: r[7],
                transport_no: r[8],
                box_no: r[9],
                insure_amount: r[10] + '万', // 保险金额
                premium: r[11], // 保费
                // fee_rate: 
            }
        })
        return arr
    }

    /**
     * Parser records
     * @param {Object} records 
     */
    parse (records) {
        // 排除标题&保单号&起始是“合计”的字样
        this.records = records.data.filter((record, index) => {
            if (index >= 3 && record.length > 0 && record[0] !== '合计') {
                return true
            }
        })
        return this.toArray()
    } 
}

module.exports = YangPuParser