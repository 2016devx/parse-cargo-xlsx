let parseDate = num => {
    // 解决node-xlsx读取时间的问题
    //  @link https://segmentfault.com/q/1010000008104936
    let d = new Date(1900, 0, num)
    let year = d.getFullYear()
    let month = d.getMonth() + 1
    let date = d.getDate() - 1
    return [
        year,
        month < 10 ? '0' + month : month,
        date < 10 ? '0' + date : date
    ].join('-')
}

let uniq = (arr) => {
    let cache = {}
    let ret = []
    arr.map((item, index) => {
        if(cache && !cache[item._id]) {
            cache[item._id] = true
            ret.push(item)
        }
    })
    return ret
}
module.exports = {
    parseDate: parseDate,
    uniq: uniq
}