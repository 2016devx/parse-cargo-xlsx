var express = require('express');
var chalk = require('chalk');
var router = express.Router();
let Extra = require('../src/Extra');
let YangPuParser = require('../src/parser/yangpu.parser');

let ItemModal = require('../src/models/records');
// let util = require('../src/util');
// let filterProps = util.filterProps;
let fs = require('fs');
let xlsx = require('node-xlsx');
let async = require('async');
let _ = require('lodash');
let util = require('../src/util/index');
let companyMap = require('../src/config/companyMap');

/**
 * Module:multer
 * multer is middleware used to handle multipart form data
 */
let multer = require('multer');
let multerUpload = multer({
	dest: 'upload/'
});

/* 插入数据. */
router.post('/',  multerUpload.any(), function(req, res, next) {
  console.log(chalk.white.bgCyan.bold('CREATE RECORDS'))
  console.log('req', req.files)

  let file = req.files[0]

  try {
    // 直接用file.path，不要拼接字符串，各个平台路径可能不一样
    let workbook = xlsx.parse(fs.readFileSync(file.path))

    let reducer = (accumulator, currentWorksheet) => {
        return accumulator.data + currentWorksheet.data
    }
    
    let records = workbook.reduce(reducer)

    if (workbook) {
        let yangpuParser = new YangPuParser()
        let extra = new Extra()
        extra.setRecords(records)
        extra.setStrategy(yangpuParser)
        let result = extra.getResult()

        ItemModal.insertMany(result, (err, docs) => {
          if (err) {
            console.log(chalk.bgRed.bold('插入数据失败'))
            throw new Error(err)
          } else {
            res.json({
              response: {
                head: {
                  errcode: "00000"
                },
                body: {
                  data: docs
                }
              }
            })
          }
        })
    }
  } catch(e) {
    console.log(chalk.white.bgCyan.bold(e))
    res.json({
      response: {
        head: {
          errcode: "11111"
        },
        body: {
          msg: 'Excel文件解析出错'
        }
      }
    })
  }
});

/**
 * 模糊查询接口
 */
router.post('/query', function(req, res, next) {
  console.log(chalk.white.bgCyan.bold('::QUERY RECORDS::'));
  console.log(chalk.white.bgBlack.bold('REQUEST BODY DATA'));
  
  // 传入的数组
  let d = req.body.request.body.data;
  let arr = d.items;
  let start_run_date = d.start_run_date;
  let company = companyMap[d.company];

  console.log(arr, start_run_date, company);

  var newArray = new Array(arr.length);
  async.eachOf(arr, (item, index, callback) => {
    console.log('INDEX: ', index);

    // 获取要查询的关键属性
    let box_no = item.box_no;
    let transport_no = item.transport_no;

    ItemModal.find({
      insured_name: company,
      start_run_date: {
        // start_run_date
        // @note
        // 调试
        $gt: '2018-07-20',
      },
      $or: [
        {
          box_no: box_no
        },
        {
          transport_no: transport_no
        }
      ]
    }, function(err, docs) {
      if(err) callback(err)

      newArray[index] = docs
      callback()
    }); // End find
  }, (err) => {
    if(err) {
      console.log(err);
      // 响应失败
      res.json({
        response: {
          head: {},
          body: {
            err: '查询失败'
          }
        }
      });
    } else {
      // 响应成功
      res.json({
        response: {
          head: {},
          body: {
            data: util.uniq(_.flattenDeep(newArray))
          }
        }
      });
    }
  }) // End Callback
}); // End /query

// 删除Item的路由
router.delete('/:id', function(req, res, next) {
  // 删除数据项的id
  var id = req.params.id;
  // LOG
  console.log(chalk.white.bgCyan.bold('query id::', id));

  /**
   * @doc
   * http://mongoosejs.com/docs/api.html#model_Model.findOneAndDelete
   * https://cnodejs.org/topic/544bb350b379fed26548a557
   */
  ItemModal.findByIdAndDelete(id, function(err, doc){
    if (err) {
      console.log(chalk.white.bgRed.bold(err));
      res.json({
        data: '删除失败'
      })
    }
    res.json({
      data: doc
    });
  });
});

module.exports = router;