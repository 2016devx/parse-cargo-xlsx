/**
 * 辅助函数模块
 */
const EXCEL_CONTENT_TYPE =
  "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

let isExcelInstalled = file => {
  return file.type !== "";
};

/**
 * 判断当前传入的文件是否为Excel文件
 */
let isExcelFile = file => {
  return file.type === EXCEL_CONTENT_TYPE;
};

/**
 * 获取传入Excel文件相似项的id
 * @param {Array} arr 要比较的数组
 * @param {Function} comparator 比较器
 */
let unique = (arr, comparator) => {
  // 记录当前数组的长度
  let len = arr.length;
  // 最终的结果, 用于标记索引
  let result = {
    // RECORD_ID: TRUE
  };
  //计数器
  let i;
  let j;

  // 双重循环
  for (i = 0; i < len; i++) {
    for (j = i + 1; j < len; j++) {
      // 相同就跳过
      if (comparator(arr[i], arr[j])) {
        result[arr[i]._id] = true;
        result[arr[j]._id] = true;
        j = ++i;
      } // if判断结束
    } // 退出内部循环
  } // 退出外部循环
  return result;
};

/**
 * 比10大吗？
 * @param {Number} num 数字
 */
let isGreatThanTen = num => {
  return parseInt(num, 10) < 10 ? "0" + num : num;
};

/**
 * 将日期解析为`XXXX-YY-DD`的格式
 */
let parseDate = (year, month, date) => {
  return [year, isGreatThanTen(month), isGreatThanTen(date)].join("-");
};

/**
 * 将日期后退几天
 * @param {String} dateString 日期（格式：XXXX-YY-DD）
 * @param {Number} day 天数
 */
let subtractDate = (dateString, day) => {
  let d = new Date(dateString);
  d.setDate(d.getDate() - day);
  let year = d.getFullYear();
  let month = d.getMonth() + 1;
  let date = d.getDate();
  return parseDate(year, month, date);
};

/**
 * 删除相同数据
 * @param {Object} state Vue状态对象
 * @param {Number} id 记录id
 */
let removeRepeatRecord = (state, id) => {
  if (state && state.hitIds[id]) {
    delete state.hitIds[id];
  }
};

/**
 * 重置hitIds
 * @param {Object} state Vuex状态
 */
let resetHitIndexObj = state => {
  state.hitIds = {};
};

/**
 * 映射记录的查询数据
 * @param {Array} records 要查询的记录
 * @return {Array} result 映射后的记录数据
 */
let mapRecords = records => {
  let result = records.map(item => {
    return {
      box_no: item.box_no,
      transport_no: item.transport_no
    };
  });
  return result;
};

/**
 * 根据value值查找出Index
 * @param {Array} arr 目标数组
 * @param {Any} value 要查找的值
 * @param {String} prop 要查找的属性,默认值为value
 */
var findIndexByValue = (arr, value, prop = "value") => {
  var result = null;
  for (let index = 0, len = arr.length; index < len; index++) {
    if (arr[index][prop] === value) {
      result = index;
      break;
    }
  }
  return result;
};

export {
  isExcelFile,
  unique,
  parseDate,
  subtractDate,
  removeRepeatRecord,
  mapRecords,
  resetHitIndexObj,
  isExcelInstalled,
  findIndexByValue
};
