// 引用UIkit模块
import UIkit from "uikit";
import Icons from "uikit/dist/js/uikit-icons";

// 加载Icons组件
UIkit.use(Icons);

export default UIkit;
