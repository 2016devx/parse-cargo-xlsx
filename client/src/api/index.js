/**
 * API入口文件
 */
import axios from "axios";
let HOST = "http://localhost:3000";
let API_CREATE_ITEMS = HOST + "/upload";
let API_SEARCH_ITEMS = HOST + "/upload/query";
let API_DELETE_ITEM = HOST + "/upload";

/**
 * 用于解析Excel数据的接口
 * @param {FormData} fd 要上传的FormData对象
 */
let parseEXCEL = fd => {
  let requestObj = fd;
  return axios.post(API_CREATE_ITEMS, requestObj);
};

/**
 * 用于删除Item的接口
 * @param {String} id ItemId
 */
let deleteItem = id => {
  return axios.delete(`${API_DELETE_ITEM}/${id}`);
};

/**
 * 查询相似的项目
 * @param {Array} items 数据项
 * @return {Promise} Axios Request
 */
let searchItems = ({ start_run_date, company, items }) => {
  return axios.post(API_SEARCH_ITEMS, {
    request: {
      head: {},
      body: {
        data: {
          items,
          company,
          start_run_date
        }
      }
    }
  });
};

export { parseEXCEL, searchItems, deleteItem };
