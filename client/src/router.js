/**
 * Vue-Router入口模块
 */
import Vue from "vue";
import Router from "vue-router";
// import store from "@/store";
import BeforeUpload from "@/views/BeforeUpload.vue";

Vue.use(Router);

let router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "before-upload",
      component: BeforeUpload
    },
    {
      path: "/after-upload",
      name: "after-upload",
      component: () =>
        import(/* webpackChunkName: "about" */ "@/views/AfterUpload.vue")
    }
  ]
});

// router.beforeEach((to, from, next) => {
//   if (to.path === "/after-upload" && !store.state.parseSuccessful) {
//     next("/");
//   }
//   next();
// });

export default router;
