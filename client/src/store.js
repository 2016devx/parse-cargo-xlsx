/**
 * Vuex状态管理模块
 */
import Vue from "vue";
import Vuex from "vuex";
import { parseEXCEL, deleteItem, searchItems } from "@/api";
import { unique, removeRepeatRecord, resetHitIndexObj } from "@/util";
import router from "./router";
import UIkit from "@/vendor";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    // 此状态用于标记文件是否解析正确
    parseSuccessful: false,
    records: [],
    hitIds: null,
    company: "yangpu",
    options: [
      { text: "洋浦中惠物流有限公司", value: "yangpu" },
      { text: "Google Ltd", value: "google" }
    ]
  },
  mutations: {
    changeParseSuccessful(state, status) {
      state.parseSuccessful = status;
    },
    changeRecords(state, records) {
      state.records = records;
    },
    changeHitIds(state, obj) {
      state.hitIds = obj;
    },
    changeCompany(state, paylod) {
      state.company = paylod.company;
    }
  },
  actions: {
    /**
     * 更改解析成功状态
     */
    changeParseSuccessful(context, file) {
      // 请求Ajax
      return parseEXCEL(file)
        .then(response => {
          console.log(response);
          context.commit("changeParseSuccessful", true);
          let r = response.data.response;
          let h = r.head;
          let b = r.body;
          if (h.errcode === "00000") {
            router.push({
              name: "after-upload"
            });
            context.commit("changeRecords", b.data);
            context.commit(
              "changeHitIds",
              unique(b.data, (obj1, obj2) => {
                return (
                  obj1.box_no === obj2.box_no &&
                  obj1.transport_no === obj2.transport_no
                );
              })
            );
          } else {
            throw new Error("请求失败，请稍后重试");
          }
        })
        .catch(err => {
          console.log(err);
        });
    },

    searchLike(context, { start_run_date, company, items }) {
      console.log(start_run_date, items, company);
      searchItems({ start_run_date, items, company })
        .then(r => {
          resetHitIndexObj(context.state);
          let data = r.data.response.body.data;
          context.commit("changeRecords", data);
        })
        .catch(err => {
          console.log(err);
        });
    },

    /**
     * 请求后端删除Item & 同步UI数据
     */
    deleteItem(context, payload) {
      let id = payload.id;
      let index = payload.index;

      console.log(`RECORD ID: ${id}`);

      deleteItem(id)
        .then(r => {
          console.log(r.data);

          let state = context.state;

          removeRepeatRecord(state, id);

          // 从起始位置index删除一个元素
          state.records.splice(index, 1);

          UIkit.notification("删除成功");
        })
        .catch(err => {
          console.log(err);
          UIkit.notification("删除失败");
        });
    }
  }
});
